$(document).ready(function(){
    $("p").dblclick(function(){
        $(this).hide();
    });
	$("#p1").mouseenter(function(){
	    alert("You entered p1!");
	});
	$("#p1").mouseleave(function(){
    alert("Bye! You now leave p1!");
	});
	$("#p2").hover(function(){
    alert("You entered p1!");
	},
	function(){
	    alert("Bye! You now leave p1!");
	});
	$("input").focus(function(){
        $(this).css("background-color", "#cccccc");
    });
    $("input").blur(function(){
        $(this).css("background-color", "#ffffff");
    });


    $("h2").on({
        mouseenter: function(){
            $(this).css("background-color", "lightgray");
        },  
        mouseleave: function(){
            $(this).css("background-color", "lightblue");
        }, 
        click: function(){
            $(this).css("background-color", "yellow");
        }  
    });

    $("#hide").click(function(){
        $("h3").hide();
    });
    $("#show").click(function(){
        $("h3").show();
    });
    $("button").click(function(){
    $("p").hide(1000); //tốc độ ẩn
    //$("p").show(1000);
    //$("p").toggle(); Các phần tử được hiển thị là các phần tử ẩn và ẩn được hiển thị
    });

    $("button").click(function(){
        $("#div1").fadeIn();
        $("#div2").fadeIn("slow");
        $("#div3").fadeIn(3000);
    });

    //$("button").click(function(){
        //$("#div1").fadeOut();
        //$("#div2").fadeOut("slow");
        //$("#div3").fadeOut(3000);
    //});


    //$("button").click(function(){
    //$("#div1").fadeToggle();
    //$("#div2").fadeToggle("slow");
    //$("#div3").fadeToggle(3000);
    //});


    //$("button").click(function(){   click vào làm mờ
        //$("#div1").fadeTo("slow", 0.15);
        //$("#div2").fadeTo("slow", 0.4);
        //$("#div3").fadeTo("slow", 0.7);
    //});


    $("#flip").click(function(){
        $("#panel").slideDown("slow");
    });


    //$("#flip").click(function(){
    //$("#panel").slideUp();
	//});

	//$("#flip").click(function(){
    //$("#panel").slideToggle();
	//});


	$("button").click(function(){
        $("div").animate({right: '0px',
    					opacity: '0.5',
    					heght:'150px',
    					width: '150px'}); //height: '+=150px',width: '+=150px'
    					//height: 'toggle' giá trị ẩn rồi hiển thị
    });


	//$("button").click(function(){
        //var div = $("div");
        //div.animate({height: '300px', opacity: '0.4'}, "slow");
        //div.animate({width: '300px', opacity: '0.8'}, "slow");
        //div.animate({height: '100px', opacity: '0.4'}, "slow");
        //div.animate({width: '100px', opacity: '0.8'}, "slow");
    //});

    //$("button").click(function(){
        //var div = $("div");  
        //div.animate({left: '100px'}, "slow");
        //div.animate({fontSize: '3em'}, "slow");
    //});

    $("button").click(function(){
        var div = $("div");  
        div.animate({left: '100px'}, "slow");
        div.animate({fontSize: '3em'}, "slow");
    });



    $("#flip").click(function(){ // dừng việc slide thả xuống
        $("#panel").slideDown(5000);
    });
    $("#stop").click(function(){
        $("#panel").stop();
    });



    //$("button").click(function(){             chức năng sẽ được thực hiện sau khi hiệu ứng giấu được hoàn thành
    //$("p").hide("slow", function(){
        //alert("The paragraph is now hidden");
    //});
	//});

	//không có tham số gọi lại và hộp cảnh báo sẽ được hiển thị trước khi hiệu ứng ẩn được hoàn tất
	//$("button").click(function(){
    //$("p").hide(1000);
    //alert("The paragraph is now hidden");
	//});

	//$("button").click(function(){
        //$("#p1").css("color", "red").slideUp(2000).slideDown(2000);
    //}); kết hợp các phương thức css (), slideUp () và slideDown (). Phần tử "p1" đầu tiên chuyển thành màu đỏ, sau đó nó trượt lên, và sau đó nó trượt xuống


    //minh họa làm thế nào để có được nội dung với các phương pháp jQuery text () và html ()
    $("#btn1").click(function(){
    alert("Text: " + $("#test").text());
	});
	$("#btn2").click(function(){
    alert("HTML: " + $("#test").html());
	});


	//hiện giá trị trong ô text input
	$("button").click(function(){
        alert("Value: " + $("#test").val());
    });


    //lấy giá trị href trong một liên kết
    $("button").click(function(){
        alert($("#w3s").attr("href"));
    });


    //cách thiết lập nội dung với các phương thức jQuery text (), html () và val ()
    $("#btn1").click(function(){
    $("#test1").text("Hello world!");
	});
	$("#btn2").click(function(){
	    $("#test2").html("<b>Hello world!</b>");
	});
	$("#btn3").click(function(){
    $("#test3").val("Dolly Duck");
	});


	//minh họa văn bản () và html () với chức năng gọi lại
	$("#btn1").click(function(){
    $("#test1").text(function(i, origText){
        return "Old text: " + origText + " New text: Hello world!(index: " + i + ")"; 
    });
	});

	$("#btn2").click(function(){
    $("#test2").html(function(i, origText){
        return "Old html: " + origText + " New html: Hello <b>world!</b>(index: " + i + ")"; 
    });


    //cách thay đổi (đặt) giá trị của thuộc tính href trong một liên kết:
    $("button").click(function(){
    $("#w3s").attr("href", "https://www.w3schools.com/jquery");
	});

    //cách đặt cả thuộc tính href và title cùng một lúc
    $("button").click(function(){
    $("#w3s").attr({
        "href" : "https://www.w3schools.com/jquery",
        "title" : "W3Schools jQuery Tutorial"
    });
	});


	//chèn thêm nội dung vào sau/ dưới
	$("#btn1").click(function(){
        $("p").append(" <b>Appended text</b>.");
    });

    $("#btn2").click(function(){
        $("ol").append("<li>Appended item</li>");
    });


    //chèn thêm nội dung vào trước/ trên
    $("#btn1").click(function(){
        $("p").prepend("<b>Prepended text</b>. ");
    });
    $("#btn2").click(function(){
        $("ol").prepend("<li>Prepended item</li>");
    });

    //đặt giá trị màu nền cho ALL các phần tử phù hợp
    $("button").click(function(){
        $("p").css("background-color": "yellow", "font-size": "200%");
    });


});
//chèn thêm một số phần tử mới
function appendText() {
    var txt1 = "<p>Text.</p>";              // Create text with HTML
    var txt2 = $("<p></p>").text("Text.");  // Create text with jQuery
    var txt3 = document.createElement("p");
    txt3.innerHTML = "Text.";               // Create text with DOM
    $("body").append(txt1, txt2, txt3);     // Append new elements
}